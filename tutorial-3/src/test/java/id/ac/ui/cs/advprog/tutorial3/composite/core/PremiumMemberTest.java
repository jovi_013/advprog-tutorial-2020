package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        member.addChildMember(new OrdinaryMember("tes", "tes"));
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member tmp = new OrdinaryMember("tes", "tes");
        member.addChildMember(tmp);
        assertEquals(1, member.getChildMembers().size());
        member.removeChildMember(tmp);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member tmp = new PremiumMember("premium", "premium");
        tmp.addChildMember(new OrdinaryMember("tes", "tes"));
        tmp.addChildMember(new OrdinaryMember("tes", "tes"));
        tmp.addChildMember(new OrdinaryMember("tes", "tes"));
        tmp.addChildMember(new OrdinaryMember("tes", "tes"));
        assertEquals(3, tmp.getChildMembers().size());

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Budi", "Master");
        assertEquals("Master", master.getRole());
        master.addChildMember(new OrdinaryMember("tes", "tes"));
        master.addChildMember(new OrdinaryMember("tes", "tes"));
        master.addChildMember(new OrdinaryMember("tes", "tes"));
        master.addChildMember(new OrdinaryMember("tes", "tes"));
        assertEquals(4, master.getChildMembers().size());

    }
}
