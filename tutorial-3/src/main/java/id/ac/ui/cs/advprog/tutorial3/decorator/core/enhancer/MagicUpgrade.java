package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    int upgradeValue;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
        this.upgradeValue = new Random().nextInt(6) + 15;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if (weapon == null) {
            return upgradeValue;
        }
        return upgradeValue + weapon.getWeaponValue();

    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Magic " + weapon.getDescription();
    }
}
