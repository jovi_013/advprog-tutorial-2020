package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int upgradeValue;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
        this.upgradeValue = new Random().nextInt(6) + 50;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(weapon == null) {
            return upgradeValue;
        }
        return upgradeValue + weapon.getWeaponValue();

    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Chaos " + weapon.getDescription();
    }
}
