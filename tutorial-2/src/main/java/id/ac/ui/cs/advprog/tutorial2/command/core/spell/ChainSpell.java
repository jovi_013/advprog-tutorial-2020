package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me

    ArrayList<Spell> spellArrayList;

    public ChainSpell(ArrayList<Spell> spellArrayList) {
        this.spellArrayList = spellArrayList;
    }

    @Override
    public void cast() {
        for (Spell s : this.spellArrayList) {
            s.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = spellArrayList.size()-1; i >= 0; i--) {
            spellArrayList.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
