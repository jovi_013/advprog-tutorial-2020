package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    HolyGrail holyGrail;

    @BeforeEach
    public void setUp() {
        holyGrail = Mockito.mock(HolyGrail.class);
    }

    @Test
    public void testMakeAWish() {
        holyGrail.makeAWish("test");
        verify(holyGrail, atLeast(1)).makeAWish("test");
    }

    @Test
    public void testGetHolyWish() {
        Mockito.doCallRealMethod().when(holyGrail).getHolyWish();
        HolyWish test = holyGrail.getHolyWish();
        assertNotNull(test);
    }

}
