package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.armor;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MetalArmorTest {

    Armor metalArmor;

    @BeforeEach
    public void setUp(){
        metalArmor = new MetalArmor();
    }

    @Test
    public void testToString(){
        // TODO create test
        String nama = metalArmor.getName();
        assertEquals("Metal Armor", nama);
    }

    @Test
    public void testDescription(){
        // TODO create test
        String desc = metalArmor.getDescription();
        assertEquals("A metal armor", desc);
    }
}
