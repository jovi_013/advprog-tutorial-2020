package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        lordranAcademy = new LordranAcademy();
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");

    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight.getArmor() != null && majesticKnight.getWeapon() != null);
        assertTrue(metalClusterKnight.getArmor() != null && metalClusterKnight.getSkill() != null);
        assertTrue(syntheticKnight.getSkill() != null && syntheticKnight.getWeapon() != null);

    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        majesticKnight.setName("maj");
        metalClusterKnight.setName("met");
        syntheticKnight.setName("syn");
        assertEquals("maj", majesticKnight.getName());
        assertEquals("met", metalClusterKnight.getName());
        assertEquals("syn", syntheticKnight.getName());

    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertTrue(majesticKnight.getArmor().getDescription().equals("A shining armor") && majesticKnight.getWeapon().getDescription().equals("A shining buster"));
        assertTrue(metalClusterKnight.getArmor().getDescription().equals("A metal armor") && metalClusterKnight.getSkill().getDescription().equals("So much pain"));
        assertTrue(syntheticKnight.getSkill().getDescription().equals("A shining force") && syntheticKnight.getWeapon().getDescription().equals("A thousand jacker"));

    }
}
